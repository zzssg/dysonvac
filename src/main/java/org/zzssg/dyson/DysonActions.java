package org.zzssg.dyson;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

public class DysonActions {
    final long timeoutInSeconds = 5;
    private WebDriver driver;
    private WebDriverWait wait;

    public DysonActions(WebDriver inDriver) {
        driver = inDriver;
        wait = new WebDriverWait(driver, timeoutInSeconds);

    }

    @Step("Load yandex page")
    public void loadYandexPage() {
        driver.get("http://ya.ru");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='text']")));
        takeScreenShot();
    }

    @Step("Search for text")
    public void doSearch(String searchString) {
        driver.findElement(By.xpath("//input[@id='text']")).sendKeys(searchString);
        takeScreenShot();
        driver.findElement(By.xpath("//div[@class='search2__button']")).click();
    }

    @Step("Click the link found")
    public void clickLinkSpecified(String linkText) {
        WebElement searchResultsHolder = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='main__content']")));
        WebElement linkToShop = searchResultsHolder.findElement(By.xpath("//li[@class='serp-item' and not(@aria-label)]//*//a[contains(@href,'" + linkText + "')]"));
        takeScreenShot();
        assertNotNull("Can't find the link by text given", linkToShop);
        linkToShop.click();
    }

    @Step("Find the item specified")
    public void findItemSpecified(String itemText) {
        this.switchToLastTab();
        takeScreenShot();
        WebElement result = wait
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'main-container')]")))
                .findElement(By.xpath("//a[contains(text(), '" + itemText + "')]"));
        assertNotNull("Should be '" + itemText + "' item found", result);
    }

    @Step("Navigate the css menu")
    public void navigateMenu() {
        Actions actions = new Actions(driver);
        actions
                .moveToElement(driver.findElement(By.xpath("//span[contains(text(), 'Ассортимент')]")))
                .moveToElement(driver.findElement(By.xpath("//span[contains(text(), 'Насадки и аксессуары')]")))
                .click()
                .perform();
        assertEquals("Page URL should be different", "http://shop.dyson.ru/accessories/", driver.getCurrentUrl());
    }

    @Step("Check that product exists")
    public void checkThatItemExists(String itemDescription){
        WebElement finalDestination = driver.findElement(By.xpath("//h5[@class='product-name']//a[contains(text(), '" + itemDescription + "')]"));
        takeScreenShot();
        assertNotNull("Can't find '" + itemDescription + "' on the page", finalDestination);
    }

    @Attachment
    @Step
    public byte[] takeScreenShot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    public void switchToLastTab() {
        List<String> tabs = new ArrayList<> (driver.getWindowHandles());
        if (tabs.size() > 1)
            driver.switchTo().window(tabs.get(tabs.size() - 1));
    }
}
