package org.zzssg.dyson.tests;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.chrome.ChromeDriver;
import org.zzssg.dyson.DysonActions;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DysonTest {

    private DysonActions actions;

    @Before
    public void setup() {
        ChromeDriverManager.getInstance().setup();
        actions = new DysonActions(new ChromeDriver());
    }

    @Test
    public void doTest() {
        actions.loadYandexPage();
        actions.doSearch("пылесос дайсон");
        actions.clickLinkSpecified("shop.dyson.ru");
        actions.findItemSpecified("Small Ball Multifloor");
        actions.navigateMenu();
        actions.checkThatItemExists("Фильтр для очистителя воздуха");
    }

}
